
//메일 정규식 체크
var email_chk = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
//문의 메일 접수
function send_consulting() {
    if ($("#c_name").val() == "" || $("#c_name").val() == null) {
       
        alert("이름을 확인해 주세요");
        $("#c_name").focus();
        return false;
    }
    

    if ($("#c_mail").val() == "" || $("#c_mail").val() == null || !email_chk.test($("#c_mail"))) {
        alert("이메일을 확인해주세요.");
        console.log("이메일을 확인해주세요.");
        $("#c_mail").focus();
        return false;
    } 

    $.ajax({
        type: "POST",
        url: "/Board/send_consulting",
        data: {
            
            iui_inquirytype: $(":input[name=type]:radio:checked").val(),
            iui_name: $("#c_name").val(),
            iui_mail: $("#c_mail").val(),
            iui_content: $("#c_text").val(),
        },
        cache: false,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        xhrFields: { withCredentials: true },
        crossDomain: true,
        success: function (json) {
            if (json.Msg == "success") {
                alert("문의 접수가 완료 되었습니다.");
                console.log("문의 접수가 완료 되었습니다.");
                window.location.reload();
            }
        },
        beforeSend: function () {

        },
        complete: function () {

        }
    });

}