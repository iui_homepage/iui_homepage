$(function () {
    var winwid = $(window).width();
    var winhei = $(window).height();

    $(".mainpage main").height(winhei);

    // gnb

    $(".pc_gnb .main").hover(function () {
        $(".sub", this).stop().slideDown(500);
    }, function () {
        $(".sub", this).stop().slideUp(500);
    });

    $(".mm_btn").click(function () {
        if ($(this).hasClass("tog")) {
            $(this).removeClass("tog");
            $(".m_gnb").animate({ right: -350 });
            $(".m_gnb").fadeOut();
        } else {
            $(".m_gnb").fadeIn();
            $(".m_gnb").animate({ right: 0 });
            $(this).addClass("tog");
        }
    });

    $(window).scroll(function () {
        if ($("html, body").scrollTop() == 0) {
            $("header").removeClass("rev");
        } else {
            $("header").addClass("rev");
        }
    });


    // works 

    var wi = $(".work_list .item").width();
    $(".work_list .item").height(wi * 4 / 3);

    $(".work_list .item").hover(function () {
        $(".overlay", this).stop().fadeIn();
        $(".textwrap", this).stop().animate({ top: "50%" });
    }, function () {
        $(".overlay", this).stop().fadeOut();
        $(".textwrap", this).stop().animate({ top: "60%" });
    });


    // float menu 

    $(".ctt_float .cttbtn").mouseenter(function () {
        $(".ctt_float .ctt_fmenu").stop().fadeIn();
        $(this).addClass("hover");
    });

    $(".ctt_float").mouseleave(function () {
        $(".ctt_float .ctt_fmenu").stop().fadeOut();
        //        $(".ctt_float .cttbtn").removeClass("hover");
    })


    // contact button

    $(".ctt_float .cttbtn #ctcb").click(function () {
        $(".ctt_float .cttbtn").hide();
        $(".ctt_float .ctt_category").fadeIn();
    });

    $(".ctt_float .ctt_category button.mail").click(function () {
        $(".ctt_float .ctt_category").hide();
        $(".ctt_float .ctt_detail").fadeIn();
    });

    $(".ctt_float .heads .txt .close, .ctt_float .btnset .cancel, .ctt_float .deco1").click(function () {
        $(".ctt_float .ctt_category, .ctt_float .ctt_detail").hide();
        $(".ctt_float .cttbtn").show();
    });


    // top button

    $(".go_top button").click(function () {
        $("html, body").stop().animate({ scrollTop: 0 }, 800);
    });

    $(window).scroll(function () {
        if ($("html, body").scrollTop() == 0) {
            $(".go_top").fadeOut();
        } else {
            $(".go_top").fadeIn();
        }
    });


    // innovation tab 

    $(".innov_cont .pw_tabs .buts li a").click(function () {
        $(".innov_cont .pw_tabs .buts li").removeClass("on");
        $(this).parent("li").addClass("on");
        $(".innov_cont .pw_tabs .tabs > li").height(0);
        $($(this).attr("href")).height("auto");

        return false;
    });


    // text ellipsis 

    var ellipsisText = function (e, etc) {
        var wordArray = e.innerHTML.split(" ");
        while (e.scrollHeight > e.offsetHeight) {
            wordArray.pop();
            e.innerHTML = wordArray.join(" ") + (etc || "...");
        }
    };

    [].forEach.call(document.querySelectorAll(".entry .ent_txt .tit_detail"), function (elem) {
        ellipsisText(elem);
    });
});