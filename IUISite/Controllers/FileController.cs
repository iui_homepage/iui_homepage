﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static System.IO.File;

using IUISite.Models;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk;
using System.IO;
using System.Text.RegularExpressions;

namespace IUISite.Controllers
{
    public class FileController : BaseController
    {
       

        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload()
        {
            var files = Request.Files;
            bool success = false;
            string url = "";
            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];
                string name = file.FileName;
                if (!string.IsNullOrEmpty(name))
                {
                    string expand = name.Substring(name.LastIndexOf('.'));
                    name = name.Replace(expand, DateTime.Now.ToString("yyyyMMddhhmmss") + expand);
                    file.SaveAs(Server.MapPath("~/static/uploads/" + name));
                    success = true;
                }
#if DEBUG
                url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + name;

                //로컬로 테스트 할 시 사용할 url
                //url = Server.MapPath("~/static/uploads/" + name);
#else
                url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Real"] + "/static/uploads/" + name;

#endif
            }
            return Json(new { url = url, success = success });
        }

        
        //하일. 웹 에서 파일 링크 클릭시 호출.
        public FileResult FileDownload(string url, string name)
        {
            url = Server.MapPath("~/static/uploads/" + url.Substring(url.LastIndexOf('/') + 1));
            return File(ReadAllBytes(url), System.Net.Mime.MediaTypeNames.Application.Octet, name);
        }


        [HttpPost]
        [AllowCrossSiteJson]
        //Works 리스트 배경 이미지 업로드
        public ActionResult FileUpload_Img(iui_worksmodel worksmodel)
        {
            OrganizationServiceProxy serviceProxy = GetCRMService();
            IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            //var originalDirectory = new DirectoryInfo(string.Format("{0}Image", Server.MapPath(@"\")));
                            //var originalDirectory = new DirectoryInfo(Server.MapPath("/Image"));

                            //string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "upload");

                            string pathString = Server.MapPath("~/static/uploads/");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
                            //url += path;
                            // url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
                           

                            //boardinfo.fileurl = "/Image/upload/" + fileName1;
                            worksmodel.imgurl = url;
                            //boardinfo.msg = "파일업로드 성공 : " + fileName1 + " ::: " + pathString;
                            worksmodel.Msg = "파일업로드 성공 : " + Files;

                            //ku_professor new_imgurl = new ku_professor();
                            //new_imgurl.Id = new Guid(boardinfo.guid);
                            //new_imgurl.ku_imgurl = url;
                            //service.Update(new_imgurl);

                            ViewBag.imgurl = url;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString() + " ====== " + worksmodel.guid;
            }

            //return View();
            //return Json(worksmodel, JsonRequestBehavior.AllowGet);
            return Redirect("https://iui.crm5.dynamics.com//WebResources/iui_works_mainimg_upload?data=" + ViewBag.imgurl);
        }

        [HttpPost]
        [AllowCrossSiteJson]
        //Works 본문 모니터 이미지 업로드
        public ActionResult FileUpload_imgM(iui_worksmodel worksmodel)
        {
            OrganizationServiceProxy serviceProxy = GetCRMService();
            IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            //var originalDirectory = new DirectoryInfo(string.Format("{0}Image", Server.MapPath(@"\")));
                            //var originalDirectory = new DirectoryInfo(Server.MapPath("/Image"));

                            //string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "upload");

                            string pathString = Server.MapPath("~/static/uploads/");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
                            //url += path;
                            // url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;


                            //boardinfo.fileurl = "/Image/upload/" + fileName1;
                            worksmodel.imgurl = url;
                            //boardinfo.msg = "파일업로드 성공 : " + fileName1 + " ::: " + pathString;
                            worksmodel.Msg = "파일업로드 성공 : " + Files;

                            //ku_professor new_imgurl = new ku_professor();
                            //new_imgurl.Id = new Guid(boardinfo.guid);
                            //new_imgurl.ku_imgurl = url;
                            //service.Update(new_imgurl);

                            ViewBag.imgurl = url;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString() + " ====== " + worksmodel.guid;
            }

            //return View();
            //return Json(worksmodel, JsonRequestBehavior.AllowGet);
            return Redirect("https://iui.crm5.dynamics.com//WebResources/iui_works_monitor_imageurl?data=" + ViewBag.imgurl);
        }

        public string Strip(string str)
        {
            //return System.Text.RegularExpressions.Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }


    }
}