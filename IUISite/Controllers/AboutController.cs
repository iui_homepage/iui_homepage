﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using IUISite.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace IUISite.Controllers
{
    public class AboutController : BaseController
    {
        //개인정보처리방침
        public ActionResult Privacy()
        {

            return View();
        }
        //찾아오는 길
        public ActionResult Direction()
        {

            return View();
        }
        public ActionResult About()
        {

            return View();
        }

        public ActionResult News()
        {
            
            var sc = new ServiceContext(GetCRMService());

            var newslist = (from a in sc.iui_newsSet
                            where a.statuscode.Value == 1
                            select new iui_newsmodel {
                           
                        }).ToList();
            var total_all = newslist.Count;

            var newsiui = (from a in sc.iui_newsSet
                           where a.iui_category.Value == 1
                           && a.statuscode.Value == 1
                           select new iui_newsmodel
                            {

                            }).ToList();
            var total_iui = newsiui.Count;

            var newsms = (from a in sc.iui_newsSet
                          where a.iui_category.Value == 2
                           && a.statuscode.Value == 1
                          select new iui_newsmodel
                            {

                            }).ToList();
            var total_ms = newsms.Count;

            ViewBag.total_all = total_all;
            ViewBag.total_iui = total_iui;
            ViewBag.total_ms = total_ms;
            return View();
        }

        public ActionResult News_Detail() {
            var sc = new ServiceContext(GetCRMService());

            var newslist = (from a in sc.iui_newsSet
                            select new iui_newsmodel
                            {

                            }).ToList();
            var total_all = newslist.Count;

            var newsiui = (from a in sc.iui_newsSet
                           where a.iui_category.Value == 1
                           select new iui_newsmodel
                           {

                           }).ToList();
            var total_iui = newsiui.Count;

            var newsms = (from a in sc.iui_newsSet
                          where a.iui_category.Value == 2
                          select new iui_newsmodel
                          {

                          }).ToList();
            var total_ms = newsms.Count;

            ViewBag.total_all = total_all;
            ViewBag.total_iui = total_iui;
            ViewBag.total_ms = total_ms;

            return View();
        }

       
        /*------------------News ajax 부분--------------*/
        /*News all category*/
        public ActionResult News_All(iui_newsmodel newsmodel)
        {

            if (newsmodel.page == 0)
            {
                newsmodel.page = 1;
            }

            var sc = new ServiceContext(GetCRMService());
            var newslist = (from a in sc.iui_newsSet
                            where a.statecode == iui_newsState.Active
                            orderby a.iui_displayon descending
                            select new iui_newsmodel
                            {
                                guid = a.iui_newsId.ToString(),
                                iui_title = a.iui_title ?? "",
                                iui_category = a.iui_category.Value,
                                CreatedOn = a.CreatedOn.Value.ToString("yyyy-MM-dd"),
                                iui_content = a.iui_content ?? "",
                                iui_list_imgurl = a.iui_list_imgurl ?? "",
                                iui_displayon = a.iui_displayon.Value.ToString("yyyy-MM-dd")
                            }).AsEnumerable().ToArray();

            if (newsmodel != null)
            {

                // 셋팅할 페이지 수
                var page_num = 5;

                // 한 페이지에 page_num의 셋팅한 수 만큼 게시물을 뿌려줌
                newsmodel.list = newslist.Skip((newsmodel.page - 1) * page_num).Take(page_num).ToArray();
                int total_cnt = newslist.Count();


                // 전체 게시물 수를 셋팅할 페이지 수로 나눴을때 나머지가 0이 아니면
                // 게시물이 더 있는걸로 판단하고 한페이지를 더 뿌려줌
                var pagecnt = total_cnt / page_num;
                if ((total_cnt % page_num) != 0)
                {
                    pagecnt++;
                }

                newsmodel.pageCnt = pagecnt;
                newsmodel.currentPage = newsmodel.page;
                newsmodel.Msg = "success";
                newsmodel.total_cnt = total_cnt;
            }
            else
            {
                newsmodel.Msg = "fail";
            }

            return Json(newsmodel, JsonRequestBehavior.AllowGet);
        }

        /*News IUI category*/
        public ActionResult News_iui(iui_newsmodel newsmodel)
        {
            if (newsmodel.page == 0)
            {
                newsmodel.page = 1;
            }

            var sc = new ServiceContext(GetCRMService());
            var newsiui = (from a in sc.iui_newsSet
                            where a.iui_category.Value == 1
                            && a.statecode == iui_newsState.Active
                           orderby a.iui_displayon descending
                           select new iui_newsmodel
                            {
                                guid = a.iui_newsId.ToString(),
                                iui_title = a.iui_title ?? "",
                                iui_category = a.iui_category.Value,
                                CreatedOn = a.CreatedOn.Value.ToString("yyyy-MM-dd"),
                                iui_content = a.iui_content ?? "",
                                iui_list_imgurl = a.iui_list_imgurl ?? "",
                               iui_displayon = a.iui_displayon.Value.ToString("yyyy-MM-dd")
                           }).AsEnumerable().ToArray();

            if (newsmodel != null)
            {

                // 셋팅할 페이지 수
                var page_num = 5;

                // 한 페이지에 page_num의 셋팅한 수 만큼 게시물을 뿌려줌
                newsmodel.list = newsiui.Skip((newsmodel.page - 1) * page_num).Take(page_num).ToArray();
                int total_cnt = newsiui.Count();


                // 전체 게시물 수를 셋팅할 페이지 수로 나눴을때 나머지가 0이 아니면
                // 게시물이 더 있는걸로 판단하고 한페이지를 더 뿌려줌
                var pagecnt = total_cnt / page_num;
                if ((total_cnt % page_num) != 0)
                {
                    pagecnt++;
                }

                newsmodel.pageCnt = pagecnt;
                newsmodel.currentPage = newsmodel.page;
                newsmodel.Msg = "success";
                newsmodel.total_cnt = total_cnt;
            }
            else
            {
                newsmodel.Msg = "fail";
            }

            return Json(newsmodel, JsonRequestBehavior.AllowGet);
        }

        /*News MS category*/
        public ActionResult News_ms(iui_newsmodel newsmodel)
        {

            if (newsmodel.page == 0)
            {
                newsmodel.page = 1;
            }
            var sc = new ServiceContext(GetCRMService());
            var newsms = (from a in sc.iui_newsSet
                           where a.iui_category.Value == 2
                           && a.statecode == iui_newsState.Active
                          orderby a.iui_displayon descending
                          select new iui_newsmodel
                           {
                               guid = a.iui_newsId.ToString(),
                               iui_title = a.iui_title ?? "",
                               iui_category = a.iui_category.Value,
                               CreatedOn = a.CreatedOn.Value.ToString("yyyy-MM-dd"),
                               iui_content = a.iui_content ?? "",
                               iui_list_imgurl = a.iui_list_imgurl ?? "",
                              iui_displayon = a.iui_displayon.Value.ToString("yyyy-MM-dd")
                          }).AsEnumerable().ToArray();

            if (newsmodel != null)
            {

                // 셋팅할 페이지 수
                var page_num = 5;

                // 한 페이지에 page_num의 셋팅한 수 만큼 게시물을 뿌려줌
                newsmodel.list = newsms.Skip((newsmodel.page - 1) * page_num).Take(page_num).ToArray();
                int total_cnt = newsms.Count();


                // 전체 게시물 수를 셋팅할 페이지 수로 나눴을때 나머지가 0이 아니면
                // 게시물이 더 있는걸로 판단하고 한페이지를 더 뿌려줌
                var pagecnt = total_cnt / page_num;
                if ((total_cnt % page_num) != 0)
                {
                    pagecnt++;
                }

                newsmodel.pageCnt = pagecnt;
                newsmodel.currentPage = newsmodel.page;
                newsmodel.Msg = "success";
                newsmodel.total_cnt = total_cnt;
            }
            else
            {
                newsmodel.Msg = "fail";
            }

            return Json(newsmodel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult News_Detail2(iui_newsmodel newsmodel)
        {

            var sc = new ServiceContext(GetCRMService());
            var newslist = (from a in sc.iui_newsSet
                            where a.iui_newsId == new Guid(newsmodel.guid)
                            select new
                            {
                                guid = a.iui_newsId.ToString(),
                                iui_title = a.iui_title == null ? "" : a.iui_title,
                                iui_category = a.iui_category.Value,
                                CreatedOn = a.CreatedOn.Value.ToString("yyyy-MM-dd"),
                                iui_content = a.iui_content == null ? "" : a.iui_content,
                                //iui_list_imgurl = a.iui_list_imgurl == null ? "" : a.iui_list_imgurl
                                iui_displayon = a.iui_displayon.Value.ToString("yyyy-MM-dd")
                            }).AsEnumerable().FirstOrDefault();

            if (newslist != null)
            {

                //ViewBag.newslist = newslist;
                newsmodel.guid = newslist.guid;
                newsmodel.iui_content = newslist.iui_content;//내용
                newsmodel.iui_title = newslist.iui_title;//제목
                newsmodel.iui_category = newslist.iui_category;//카테고리 구분
                newsmodel.CreatedOn = newslist.CreatedOn;//작성일
                newsmodel.iui_displayon = newslist.iui_displayon;
            //    newsmodel.Msg = "success";

            //}
            //else {
            //    newsmodel.Msg = "fail";
            }
            
            return Json(newsmodel, JsonRequestBehavior.AllowGet);
        }



        /*------CRM 파일 업로드 -------------------------------------------------------------------*/


        /*파일 업로드 (서버-본문)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload(iui_newsmodel newsmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            newsmodel.imgurl = url;
                            newsmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    newsmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                newsmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(newsmodel, JsonRequestBehavior.AllowGet);
            
        }

        /*파일 업로드 (썸네일)*/
        [HttpPost]
        [AllowCrossSiteJson]
        //Works 리스트 배경 이미지 업로드
        public ActionResult FileUpload_Img(iui_newsmodel newsmodel)
        {
            OrganizationServiceProxy serviceProxy = GetCRMService();
            IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            //var originalDirectory = new DirectoryInfo(string.Format("{0}Image", Server.MapPath(@"\")));
                            //var originalDirectory = new DirectoryInfo(Server.MapPath("/Image"));

                            //string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "upload");

                            string pathString = Server.MapPath("~/static/uploads/");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
                            //url += path;
                            // url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;


                            //boardinfo.fileurl = "/Image/upload/" + fileName1;
                            newsmodel.imgurl = url;
                            //boardinfo.msg = "파일업로드 성공 : " + fileName1 + " ::: " + pathString;
                            newsmodel.Msg = "파일업로드 성공 : " + Files;

                            //ku_professor new_imgurl = new ku_professor();
                            //new_imgurl.Id = new Guid(boardinfo.guid);
                            //new_imgurl.ku_imgurl = url;
                            //service.Update(new_imgurl);

                            ViewBag.imgurl = url;
                        }
                    }
                }
                else
                {
                    newsmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                newsmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString() + " ====== " + newsmodel.guid;
            }

            //return View();
            //return Json(worksmodel, JsonRequestBehavior.AllowGet);
            return Redirect("https://iui.crm5.dynamics.com//WebResources/iui_news_list_img?data=" + ViewBag.imgurl);
        }

        public string Strip(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
            // return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            // return Regex.Replace(str,@"[<][b-z|B-Z|/](.|\n)*?[>]", "", RegexOptions.Compiled);
        }


    }
}