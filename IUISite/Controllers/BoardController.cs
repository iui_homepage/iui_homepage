﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using IUISite.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace IUISite.Controllers
{
    public class BoardController : BaseController
    {


        public ActionResult send_consulting(iui_boardmodel boardmodel)
        {

            using (var sc = new ServiceContext(GetCRMService()))
            {

                /*
                select = (from a in sc.ku_consultingSet
                              where a.ku_site.Id == new Guid(boardinfo.site_guid) &&
                                    a.ku_phone == boardinfo.phone &&
                                    a.ku_name == boardinfo.name
                              select a).AsEnumerable().FirstOrDefault();
                              */
                iui_Mailto new_mailto = new iui_Mailto();
                new_mailto.iui_inquirytype = new OptionSetValue(boardmodel.iui_inquirytype);//문의 종류
                //new_mailto.iui_inquirytype = boardmodel.iui_inquirytype;//문의 종류
                new_mailto.iui_mail = boardmodel.iui_mail == null ? "" : boardmodel.iui_mail;//메일
                new_mailto.iui_name = boardmodel.iui_name == null ? "" : boardmodel.iui_name;//문의 접수자 메일
                new_mailto.iui_content = boardmodel.iui_content == null ? "" : boardmodel.iui_content;

               
                Guid guid = GetCRMService().Create(new_mailto);

                boardmodel.Msg = "success";

            }
           

            return Json(boardmodel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Works_Project(iui_worksmodel worksmodel)
        {

            var sc = new ServiceContext(GetCRMService());
           // int total_p = 0;
            if (worksmodel.page == 0)
            {
                worksmodel.page = 1;
            }

            var workslist_p = (from a in sc.iui_worksSet
                               where a.iui_field_maintenance == false || a.iui_field_maintenance == null
                               orderby a.CreatedOn descending
                               select new iui_worksmodel
                               {
                                   guid = a.iui_worksId.ToString(),
                                   iui_title = a.iui_title ?? "",
                                   iui_type_crm = Convert.ToBoolean(a.iui_type_crm),
                                   iui_type_web = Convert.ToBoolean(a.iui_type_web),
                                   iui_type_app = Convert.ToBoolean(a.iui_type_app),
                                   iui_type_etc = a.iui_type_etc ?? "",
                                   iui_year = a.iui_year ?? "",
                                   iui_contactname = a.iui_contactname ?? "",
                                   imgurl = a.iui_main_image ?? "",
                                   iui_field_develop = Convert.ToBoolean(a.iui_field_develop)

                               }).AsEnumerable().ToArray();
           

           
            //ViewBag.workslist_p = workslist_p;
            //ViewBag.total_p = total_p;

            var page_num = 32;

            if (workslist_p != null)
            {
                worksmodel.list = workslist_p.Skip((worksmodel.page - 1) * page_num).Take(page_num).ToArray();
                int total_p = workslist_p.Count();

                var pagecnt = workslist_p.Count() / page_num;
                if ((workslist_p.Count() % page_num) != 0)
                {
                    pagecnt++;
                }
                //worksmodel.no = page_num;
                worksmodel.pageCnt = pagecnt;
                worksmodel.currentPage = worksmodel.page;
                worksmodel.Msg = "success";
                
            }
            else {
                worksmodel.Msg = "fail";
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }

       

        /*파일 업로드 (서버-본문)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload(iui_worksmodel worksmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            worksmodel.imgurl = url;
                            worksmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }

        /*파일 업로드 (서버-PC View)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload_Pc(iui_worksmodel worksmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            //url += "https://pe.iuicrm.com:9999/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            worksmodel.imgurl = url;
                            worksmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }

        /*파일 업로드 (서버-Mobile View)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload_Mobile(iui_worksmodel worksmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                           //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            worksmodel.imgurl = url;
                            worksmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }


        public string Strip(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
            // return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            // return Regex.Replace(str,@"[<][b-z|B-Z|/](.|\n)*?[>]", "", RegexOptions.Compiled);
        }

    }
}