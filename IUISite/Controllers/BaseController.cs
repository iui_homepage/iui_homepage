﻿using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IUISite.Models;

namespace IUISite.Controllers
{
    public class BaseController : Controller
    {
        // 광현. 베이스 CRM 연동 세팅
        // GET: Base
        public OrganizationServiceProxy GetCRMServiceAuth()
        {
            //로그인 인증
            ClientCredentials Credentials = new ClientCredentials();

            /* For Production start  */

            // webconfig에서 가져옴
            Credentials.UserName.UserName = System.Configuration.ConfigurationManager.AppSettings["UserName"];
            Credentials.UserName.Password = System.Configuration.ConfigurationManager.AppSettings["Password"];
#if DEBUG
            //Uri OrganizationUri = new Uri(System.Configuration.ConfigurationManager.AppSettings["CrmUrl_Dev"]);
            Uri OrganizationUri = new Uri(System.Configuration.ConfigurationManager.AppSettings["CrmUrl"]);

#else
			Uri OrganizationUri = new Uri(System.Configuration.ConfigurationManager.AppSettings["CrmUrl"]);
#endif

            /* For Production ends */

            //Credentials.UserName.UserName = "";
            //Credentials.UserName.Password = "";
            //Uri OrganizationUri = new Uri("https://iuidoosan.crm5.dynamics.com/XRMServices/2011/Organization.svc");

            Uri HomeRealmUri = null;

            using (OrganizationServiceProxy serviceProxy = new OrganizationServiceProxy(OrganizationUri, HomeRealmUri, Credentials, null))
            {
                serviceProxy.EnableProxyTypes();
                return serviceProxy;
            }
        }

        public OrganizationServiceProxy GetCRMService()
        {
            return (OrganizationServiceProxy)Session["CrmServiceProxy"];
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session["CrmServiceProxy"] == null)
            {
                // 인증 태워서 serviceProxy를 세션에 등록
                OrganizationServiceProxy serviceProxy = GetCRMServiceAuth();
                filterContext.HttpContext.Session["CrmServiceProxy"] = serviceProxy;
                base.OnActionExecuting(filterContext);
            }
            else
            {
                // 인증 살아 있는지 확인할까?
                try
                {
                    var x = new ServiceContext((IOrganizationService)filterContext.HttpContext.Session["CrmServiceProxy"]);
                }
                catch (Exception)
                {
                    // 인증 태워서 serviceProxy를 세션에 등록
                    OrganizationServiceProxy serviceProxy = GetCRMServiceAuth();
                    filterContext.HttpContext.Session["CrmServiceProxy"] = serviceProxy;
                    base.OnActionExecuting(filterContext);
                }
            }
        }

        public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var origin = filterContext.RequestContext.HttpContext.Request.Headers["Origin"];
                var allowOrigin = !string.IsNullOrWhiteSpace(origin) ? origin : "*";
                filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", allowOrigin);
                filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Credentials", "true");
                filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type, origin, authorization, accept, client-security-token");
                filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
                filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Max-Age", "1000");
                base.OnActionExecuting(filterContext);
            }
        }
    }
}