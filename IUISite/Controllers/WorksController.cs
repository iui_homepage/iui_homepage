﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using IUISite.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace IUISite.Controllers
{
    public class WorksController : BaseController
    {
        public ActionResult Works()
        {

            var sc = new ServiceContext(GetCRMService());
            int total_all = 0;
            int total_p = 0;
            int total_m = 0;
            //전체 리스트
            var workslist = (from a in sc.iui_worksSet
                             where a.statecode == iui_worksState.Active
                             orderby a.iui_year descending
                             select new iui_worksmodel
                             {

                                 guid = a.iui_worksId.ToString(),
                                 iui_title = a.iui_title ?? "",
                                 iui_type_crm = Convert.ToBoolean(a.iui_type_crm),
                                 iui_type_web = Convert.ToBoolean(a.iui_type_web),
                                 iui_type_app = Convert.ToBoolean(a.iui_type_app),
                                 iui_type_etc = a.iui_type_etc ?? "",
                                 iui_year = a.iui_year ?? "",
                                 iui_contactname = a.iui_contactname ?? "",
                                 imgurl = a.iui_main_image ?? "",
                                 iui_display_option = Convert.ToBoolean(a.iui_display_option)

                             }).ToList();
            total_all = workslist.Count;

            //구분값 프로젝트일때 리스트
            var workslist_p = (from a in sc.iui_worksSet
                               where a.iui_field_maintenance == false || a.iui_field_maintenance == null
                               && a.statecode == iui_worksState.Active
                               orderby a.iui_year descending
                               select new iui_worksmodel
                               {
                                   guid = a.iui_worksId.ToString(),
                                   iui_title = a.iui_title ?? "",
                                   iui_type_crm = Convert.ToBoolean(a.iui_type_crm),
                                   iui_type_web = Convert.ToBoolean(a.iui_type_web),
                                   iui_type_app = Convert.ToBoolean(a.iui_type_app),
                                   iui_type_etc = a.iui_type_etc ?? "",
                                   iui_year = a.iui_year ?? "",
                                   iui_contactname = a.iui_contactname ?? "",
                                   imgurl = a.iui_main_image ?? "",
                                   iui_field_develop = Convert.ToBoolean(a.iui_field_develop)

                               }).ToList();
            total_p = workslist_p.Count;

            //구분값 유지보수일떄 리스트
            var workslist_m = (from a in sc.iui_worksSet
                               where a.iui_field_maintenance == true
                               && a.statecode == iui_worksState.Active
                               orderby a.iui_year descending
                               select new iui_worksmodel
                               {
                                   guid = a.iui_worksId.ToString(),
                                   iui_title = a.iui_title ?? "",
                                   iui_type_crm = Convert.ToBoolean(a.iui_type_crm),
                                   iui_type_web = Convert.ToBoolean(a.iui_type_web),
                                   iui_type_app = Convert.ToBoolean(a.iui_type_app),
                                   iui_type_etc = a.iui_type_etc ?? "",
                                   iui_year = a.iui_year ?? "",
                                   iui_contactname = a.iui_contactname ?? "",
                                   imgurl = a.iui_main_image ?? "",
                                   iui_field_maintenance = Convert.ToBoolean(a.iui_field_maintenance)

                               }).ToList();
            total_m = workslist_m.Count;

            ViewBag.workslist = workslist;
            ViewBag.workslist_p = workslist_p;
            ViewBag.workslist_m = workslist_m;
            ViewBag.total_all = total_all;
            ViewBag.total_p = total_p;
            ViewBag.total_m = total_m;


            return View();
        }

      
       
        public ActionResult Works_Detail(string id)
        {

            var sc = new ServiceContext(GetCRMService());
            int total_all = 0;
            int total_p = 0;
            int total_m = 0;
            //전체 리스트
            var workslist = (from a in sc.iui_worksSet
                                 // where a.statecode == iui_worksState.Active
                             orderby a.CreatedOn descending
                             select new iui_worksmodel
                             {

                             }).ToList();
            total_all = workslist.Count;

            //구분값 프로젝트일때 리스트
            var workslist_p = (from a in sc.iui_worksSet
                               where a.iui_field_maintenance == false || a.iui_field_maintenance == null
                               orderby a.CreatedOn descending
                               select new iui_worksmodel
                               {
                                  
                               }).ToList();
            total_p = workslist_p.Count;

            //구분값 유지보수일떄 리스트
            var workslist_m = (from a in sc.iui_worksSet
                               where a.iui_field_maintenance == true
                               orderby a.CreatedOn descending
                               select new iui_worksmodel
                               {
                                

                               }).ToList();
            total_m = workslist_m.Count;

           


            var works = (from a in sc.iui_worksSet
                         where a.iui_worksId == new Guid(id)
                         select new iui_worksmodel
                         {
                             //guid = a.Id,
                             iui_title = a.iui_title ?? "",
                             iui_content = a.iui_content ?? "",
                             iui_contactname = a.iui_contactname ?? "",
                             iui_type_crm = Convert.ToBoolean(a.iui_type_crm),
                             iui_type_web = Convert.ToBoolean(a.iui_type_web),
                             iui_type_app = Convert.ToBoolean(a.iui_type_app),
                             iui_year = a.iui_year ?? "",
                             iui_detail_imgurl = a.iui_detail_imgurl ?? "",
                             iui_pcview = a.iui_pcview ?? "",
                             iui_mobileview = a.iui_mobileview ?? "",
                             iui_field_d365 = Convert.ToBoolean(a.iui_field_d365),
                             iui_field_design = Convert.ToBoolean(a.iui_field_design),
                             iui_field_develop = Convert.ToBoolean(a.iui_field_develop),
                             iui_field_maintenance = Convert.ToBoolean(a.iui_field_maintenance),
                             iui_field_publishing = Convert.ToBoolean(a.iui_field_publishing),
                             iui_siteurl = a.iui_siteurl ?? "",
                             iui_color = a.iui_color ?? ""

                         }).FirstOrDefault();

            if (works == null)
            {
                HttpNotFound();
            }

            ViewBag.works = works;

            ViewBag.workslist = workslist;
            ViewBag.workslist_p = workslist_p;
            ViewBag.workslist_m = workslist_m;
            ViewBag.total_all = total_all;
            ViewBag.total_p = total_p;
            ViewBag.total_m = total_m;

            return View();
        }

        /*파일 업로드 (서버-본문)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload(iui_worksmodel worksmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            worksmodel.imgurl = url;
                            worksmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }

        /*파일 업로드 (서버-PC View)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload_Pc(iui_worksmodel worksmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            //url += "https://pe.iuicrm.com:9999/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            worksmodel.imgurl = url;
                            worksmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }

        /*파일 업로드 (서버-Mobile View)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload_Mobile(iui_worksmodel worksmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                           //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            worksmodel.imgurl = url;
                            worksmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    worksmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                worksmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(worksmodel, JsonRequestBehavior.AllowGet);
        }


        public string Strip(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
            // return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            // return Regex.Replace(str,@"[<][b-z|B-Z|/](.|\n)*?[>]", "", RegexOptions.Compiled);
        }

    }
}