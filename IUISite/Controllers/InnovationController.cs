﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using IUISite.Models;


namespace IUISite.Controllers
{
    public class InnovationController : BaseController
    {
        //Innovation - Dynamics365 메뉴
        public ActionResult Dynamics365_Index()
        {

            return View();
        }
        //Index 페이지에서 D365 Trial 바로가기
        public ActionResult Dynamics365_main()
        {
            
            return View();
        }
        //Innovation - Azure 메뉴
        public ActionResult Azure_Show() {

            return View();
        }
        //Index 페이지에서 Azure 체험 페이지 바로가기
        public ActionResult Azure_main()
        {

            return View();
        }
        //Innovation - Powerplatform 메뉴
        public ActionResult BI_Show()
        {

            return View();
        }
        //Index 페이지에서 BI 체험페이지 바로가기
        public ActionResult BI_main()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Detect()
        {
            var file = Request.Files[0];
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "5e0a2eec826143a68618a2afa38d696c");

            // Request parameters
            queryString["returnFaceId"] = "true";
            queryString["returnFaceLandmarks"] = "false";
            queryString["returnFaceAttributes"] = "age,gender,smile,glasses,emotion,hair,makeup";
            queryString["recognitionModel"] = "recognition_01";
            queryString["returnRecognitionModel"] = "false";
            queryString["detectionModel"] = "detection_01";
            var uri = "https://namgunggwanghyun.cognitiveservices.azure.com/face/v1.0/detect?" + queryString;

            // Request body
            byte[] byteData = new BinaryReader(file.InputStream).ReadBytes((int)file.ContentLength);
            var content = new ByteArrayContent(byteData);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            var response = client.PostAsync(uri, content).Result;
            string result = response.Content.ReadAsStringAsync().Result;
            var serializer = new JavaScriptSerializer();
            return Json(new
            {
                success = response.IsSuccessStatusCode,
                data = serializer.DeserializeObject(result)
            });
        }



        /*------CRM 파일업로드 부분---------------------------------------------------------------------------------*/

        /*파일 업로드 (서버-본문)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload(iui_innomodel innomodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            innomodel.imgurl = url;
                            innomodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    innomodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                innomodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(innomodel, JsonRequestBehavior.AllowGet);

        }

        public string Strip(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
            // return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            // return Regex.Replace(str,@"[<][b-z|B-Z|/](.|\n)*?[>]", "", RegexOptions.Compiled);
        }


    }
}