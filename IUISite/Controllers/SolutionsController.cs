﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using IUISite.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;

namespace IUISite.Controllers
{
    public class SolutionsController : BaseController
    {
        public ActionResult Solutions()
        {

            var sc = new ServiceContext(GetCRMService());

            var slist = (from a in sc.iui_solutionsSet
                         orderby a.iui_displayon descending
                         select new iui_solutionsmodel
                         {
                             guid = a.iui_solutionsId.ToString(),
                             iui_title = a.iui_title ?? "",
                             iui_subtitle = a.iui_subtitle ?? "",
                             iui_category = Convert.ToInt32(a.iui_category.Value),
                             iui_displayon = a.iui_displayon.Value.ToString("yyyy.MM.dd"),
                             imgurl = a.iui_thumbnail,
                             iui_content = a.iui_content
                         }).ToList();

            ViewBag.slist = slist;

            return View();
        }

        public ActionResult Solution_Detail(string guid) {

            var sc = new ServiceContext(GetCRMService());

            var soldetail = (from a in sc.iui_solutionsSet
                             where a.iui_solutionsId.Equals(guid)
                             orderby a.iui_displayon descending
                             select new iui_solutionsmodel
                             {
                                 guid = a.iui_solutionsId.ToString(),
                                 iui_title = a.iui_title ?? "",
                                 iui_subtitle = a.iui_subtitle ?? "",
                                 iui_category = Convert.ToInt32(a.iui_category.Value),
                                 iui_displayon = a.iui_displayon.Value.ToString("yyyy.MM.dd"),
                                 imgurl = a.iui_thumbnail,
                                 iui_content = a.iui_content
                             }).FirstOrDefault();

            if (soldetail == null)
            {
                HttpNotFound();
            }

            ViewBag.soldetail = soldetail;

            return View();
        }

        //Solutions List Ajax
        public ActionResult Solution_List(iui_solutionsmodel solutionsmodel)
        {

            if (solutionsmodel.page == 0)
            {
                solutionsmodel.page = 1;
            }

            var sc = new ServiceContext(GetCRMService());
            var slist = (from a in sc.iui_solutionsSet
                         where a.statecode == iui_solutionsState.Active
                         orderby a.iui_displayon descending
                         select new iui_solutionsmodel
                         {
                             guid = a.iui_solutionsId.ToString(),
                             iui_title = a.iui_title ?? "",
                             iui_subtitle = a.iui_subtitle ?? "",
                             iui_category = Convert.ToInt32(a.iui_category.Value),
                             iui_displayon = a.iui_displayon.Value.ToString("yyyy.MM.dd"),
                             imgurl = a.iui_thumbnail ?? "",
                             iui_content = a.iui_content ?? ""
                         }).AsEnumerable().ToArray();

            if (solutionsmodel != null)
            {

                // 셋팅할 페이지 수
                var page_num =  4;

                // 한 페이지에 page_num의 셋팅한 수 만큼 게시물을 뿌려줌
                solutionsmodel.list = slist.Skip((solutionsmodel.page - 1) * page_num).Take(page_num).ToArray();
                int total_cnt = slist.Count();


                // 전체 게시물 수를 셋팅할 페이지 수로 나눴을때 나머지가 0이 아니면
                // 게시물이 더 있는걸로 판단하고 한페이지를 더 뿌려줌
                var pagecnt = total_cnt / page_num;
                if ((total_cnt % page_num) != 0)
                {
                    pagecnt++;
                }

                solutionsmodel.pageCnt = pagecnt;
                solutionsmodel.currentPage = solutionsmodel.page;
                solutionsmodel.Msg = "success";
                solutionsmodel.total_cnt = total_cnt;
            }
            else
            {
                solutionsmodel.Msg = "fail";
            }


            return Json(solutionsmodel, JsonRequestBehavior.AllowGet);
        }

        
        /*파일 업로드 (서버-본문)*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload(iui_solutionsmodel solutionsmodel)
        {
            //OrganizationServiceProxy serviceProxy = GetCRMService();
            //IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            string pathString = Server.MapPath("~/static/uploads");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("[solution]yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
#if DEBUG
                            url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            //url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#else
                            //url += "https://pe.konkuk.ac.kr/Image/upload/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;
#endif

                            solutionsmodel.imgurl = url;
                            solutionsmodel.Msg = "파일업로드 성공 : " + path;
                        }
                    }
                }
                else
                {
                    solutionsmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                solutionsmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString();
            }

            return Json(solutionsmodel, JsonRequestBehavior.AllowGet);
        }

        /*썸네일 파일 업로드*/
        [HttpPost]
        [AllowCrossSiteJson]
        public ActionResult FileUpload_Img(iui_solutionsmodel solutionsmodel)
        {
            OrganizationServiceProxy serviceProxy = GetCRMService();
            IOrganizationService service = (IOrganizationService)serviceProxy;

            var Files = Request.Files.Count;
            String url = "";

            try
            {
                if (Files != 0)
                {
                    for (int i = 0; i < Files; i++)
                    {
                        HttpPostedFileBase fileupload = Request.Files[i];

                        if (fileupload.FileName != null && fileupload.FileName != "")
                        {
                            //var originalDirectory = new DirectoryInfo(string.Format("{0}Image", Server.MapPath(@"\")));
                            //var originalDirectory = new DirectoryInfo(Server.MapPath("/Image"));

                            //string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "upload");

                            string pathString = Server.MapPath("~/static/uploads/");

                            var fileName1 = Path.GetFileName(DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + Strip(fileupload.FileName));

                            bool isExists = System.IO.Directory.Exists(pathString);

                            if (!isExists)
                                System.IO.Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, fileName1);
                            fileupload.SaveAs(path);

                            if (i != 0)
                            {
                                url += ",";
                            }
                            //url += path;
                            // url += "https://iui.iuicrm.com:5566/static/uploads/" + fileName1;
                            url = System.Configuration.ConfigurationManager.AppSettings["WebUrl_Dev"] + "/static/uploads/" + fileName1;


                            //boardinfo.fileurl = "/Image/upload/" + fileName1;
                            solutionsmodel.imgurl = url;
                            //boardinfo.msg = "파일업로드 성공 : " + fileName1 + " ::: " + pathString;
                            solutionsmodel.Msg = "파일업로드 성공 : " + Files;

                            //ku_professor new_imgurl = new ku_professor();
                            //new_imgurl.Id = new Guid(boardinfo.guid);
                            //new_imgurl.ku_imgurl = url;
                            //service.Update(new_imgurl);

                            ViewBag.imgurl = url;
                        }
                    }
                }
                else
                {
                    solutionsmodel.Msg = "파일없음 : " + Files;
                }
            }
            catch (Exception e)
            {
                solutionsmodel.Msg = "파일업로드 실패 : " + Files + " =====\n " + e.ToString() + " ====== " + solutionsmodel.guid;
            }

            //return View();
            //return Json(worksmodel, JsonRequestBehavior.AllowGet);
            return Redirect("https://iui.crm5.dynamics.com//WebResources/iui_thumbnail_solution?data=" + ViewBag.imgurl);
        }



        public string Strip(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
            // return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            // return Regex.Replace(str,@"[<][b-z|B-Z|/](.|\n)*?[>]", "", RegexOptions.Compiled);
        }

    }
}