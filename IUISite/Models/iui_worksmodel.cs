﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUISite.Models
{
    public class iui_worksmodel
    {
        public String guid { get; set;}
        public DateTime CreatedOn { get; set; }
        public string iui_title { get; set; }
        public string iui_content { get; set; }
        public string iui_type { get; set; }
        public string iui_year { get; set; }

        public bool iui_type_crm { get; set; }
        public bool iui_type_web { get; set; }
        public bool iui_type_app { get; set; }
        public string iui_type_etc { get; set; }

        public string imgurl { get; set; }
        public string Msg { get; set; }
        public string iui_contactname { get; set; }

        public string iui_pcview { get; set; }
        public string iui_mobileview { get; set; }
        public string iui_detail_imgurl { get; set; }

        public bool iui_field_design { get; set; }
        public bool iui_field_develop { get; set; }
        public bool iui_field_publishing { get; set; }
        public bool iui_field_d365 { get; set; }
        public bool iui_field_maintenance { get; set; }

        public string iui_siteurl { get; set; }
        public string iui_color { get; set; }
        public bool iui_display_option { get; set; }
        /*-------------------------------------------------*/
        public Array list { get; set; } // 게시판 리스트 배열

        public int page { get; set; } // 페이지 번호
        public String keyword { get; set; } // 검색어
        public String keywordType { get; set; } // 검색 종류
        public int pageCnt { get; set; } // 페이지 카운트
        public int currentPage { get; set; } // 현재 페이지

        public int total_cnt { get; set; } // 총 게시물 수

        public int total_all { get; set; }
        public int total_iui { get; set; }
        public int total_ms { get; set; }

    }
}