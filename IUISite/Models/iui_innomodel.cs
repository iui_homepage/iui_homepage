﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUISite.Models
{
    public class iui_innomodel
    {
        public Guid guid { get; set;}
        public DateTime CreatedOn { get; set; }

        public int? iui_innovation_type { get; set; }
        public int? iui_innovation_menu { get; set; }
        public int? iui_item { get; set; }

        public string iui_title { get; set; }
        public string iui_innovation_content { get; set; }
        public string iui_innovation_content2 { get; set; }

        public string imgurl { get; set; }
        public string Msg { get; set; }

    }
}