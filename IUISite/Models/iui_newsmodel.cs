﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUISite.Models
{
    public class iui_newsmodel
    {
        public string guid { get; set;}
        //public DateTime CreatedOn { get; set; }
        public string CreatedOn { get; set; }
        public string iui_displayon { get; set; }

        public int? iui_category { get; set; }

        public string iui_title { get; set; }
        public string iui_subtitle { get; set; }
        public string iui_innovation_content { get; set; }
        public string iui_videourl { get; set; }

        public string iui_list_imgurl { get; set; }
        public string imgurl { get; set; }
        public string iui_content { get; set; }

        public string Msg { get; set; }


        public Array list { get; set; } // 게시판 리스트 배열
        
        public int page { get; set; } // 페이지 번호
        public String keyword { get; set; } // 검색어
        public String keywordType { get; set; } // 검색 종류
        public int pageCnt { get; set; } // 페이지 카운트
        public int currentPage { get; set; } // 현재 페이지

        public int total_cnt { get; set; } // 총 게시물 수

        public int total_all { get; set; }
        public int total_iui { get; set; }
        public int total_ms { get; set; }

    }
}