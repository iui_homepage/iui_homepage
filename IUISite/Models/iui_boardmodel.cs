﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IUISite.Models
{
    public class iui_boardmodel
    {
        public String guid { get; set;}
        public DateTime CreatedOn { get; set; }
        public int iui_inquirytype { get; set; }
        public string iui_name { get; set; }
        public string iui_mail { get; set; }
        public string iui_content { get; set; }
        public string Msg { get; set; }
    }
}